package pl.sokol.simplequeue.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {

    public static final String MESSAGES_QUEUE_NAME = "messages";
    public static final String MESSAGES_EXCHANGE_NAME = "messages-exchange";
    public static final String MESSAGES_ROUTING_KEY_NAME = "messages-key";

    public static final String MESSAGES_DEAD_LETTER_EXCHANGE_NAME = "messages-dead-letter-exchange";
    public static final String MESSAGES_DEAD_LETTER_QUEUE_KEY_NAME = "messages-dead-letter-key";

    public static final String MESSAGES_TTL_QUEUE_NAME = "messages-ttl";

    public static final String EXP_MESSAGES_EXCHANGE_NAME = "exp-messages-exchange";

    public static final String REJECTED_MESSAGES_QUEUE_NAME = "rejected-messages";
    public static final String REJECTED_MESSAGES_EXCHANGE_NAME = "rejected-messages-exchange";
    public static final String REJECTED_MESSAGES_ROUTING_KEY_NAME = "rejected-messages-key";

    @Bean
    @Qualifier("messagesQueue")
    public Queue messagesQueue() {
        return QueueBuilder.durable(MESSAGES_QUEUE_NAME)
                           .withArgument("x-dead-letter-exchange", MESSAGES_DEAD_LETTER_EXCHANGE_NAME)
                           .withArgument("x-dead-letter-routing-key", MESSAGES_DEAD_LETTER_QUEUE_KEY_NAME)
                           .build();
    }

    @Bean
    @Qualifier("messagesTtlQueue")
    public Queue messagesTtlQueue() {
        return QueueBuilder.durable(MESSAGES_TTL_QUEUE_NAME)
                           .withArgument("x-message-ttl", 30000)
                           .withArgument("x-dead-letter-exchange", EXP_MESSAGES_EXCHANGE_NAME)
                           .withArgument("x-dead-letter-routing-key", MESSAGES_ROUTING_KEY_NAME)
                           .build();
    }

    @Bean
    @Qualifier("rejectedMessages")
    public Queue rejectedMessages() {
        return QueueBuilder.durable(REJECTED_MESSAGES_QUEUE_NAME).build();
    }

    @Bean
    @Qualifier("messagesExchange")
    public Exchange messagesExchange() {
        return new DirectExchange(MESSAGES_EXCHANGE_NAME);
    }

    @Bean
    @Qualifier("messagesDeadLetterExchange")
    public Exchange messagesDeadLetterExchange() {
        return new DirectExchange(MESSAGES_DEAD_LETTER_EXCHANGE_NAME);
    }

    @Bean
    @Qualifier("expMessagesExchange")
    public Exchange expMessagesExchange() {
        return new DirectExchange(EXP_MESSAGES_EXCHANGE_NAME);
    }

    @Bean
    @Qualifier("rejectedMessagesExchange")
    public Exchange rejectedMessagesExchange() {
        return new DirectExchange(REJECTED_MESSAGES_EXCHANGE_NAME);
    }


    @Bean
    public Binding bindingMessages(@Qualifier("messagesExchange") Exchange exchange, @Qualifier("messagesQueue") Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGES_ROUTING_KEY_NAME).noargs();
    }

    @Bean
    public Binding bindingTtlMessages(@Qualifier("messagesDeadLetterExchange") Exchange exchange, @Qualifier("messagesTtlQueue") Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGES_DEAD_LETTER_QUEUE_KEY_NAME).noargs();
    }

    @Bean
    public Binding bindingExpMessages(@Qualifier("expMessagesExchange") Exchange exchange, @Qualifier("messagesQueue") Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(MESSAGES_ROUTING_KEY_NAME).noargs();
    }

    @Bean
    public Binding bindingRejectedMessages(@Qualifier("rejectedMessagesExchange") Exchange exchange, @Qualifier("rejectedMessages") Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(REJECTED_MESSAGES_ROUTING_KEY_NAME).noargs();
    }
}