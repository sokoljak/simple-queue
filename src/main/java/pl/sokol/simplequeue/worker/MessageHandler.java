package pl.sokol.simplequeue.worker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import pl.sokol.simplequeue.config.QueueConfig;

import java.util.HashMap;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageHandler {

    private final RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = QueueConfig.MESSAGES_QUEUE_NAME)
    public void handleMessage(String msg, @Header(required = false, name = "x-death") List<HashMap<String,Object>> xDeath) {

        final Long deathCount = xDeath == null ? null : (Long) xDeath.get(0).get("count");


        if (deathCount != null && deathCount >= 2L) {
            log.info("Retry counter exceeded, sending message to rejected queue: {}", msg);
            rabbitTemplate.convertAndSend(QueueConfig.REJECTED_MESSAGES_EXCHANGE_NAME, QueueConfig.REJECTED_MESSAGES_ROUTING_KEY_NAME, msg);
            return;
        }

        if ("fail".equals(msg)) {
            throw new RuntimeException("Handling fault");
        }

        log.info("Handling message, content: {}", msg);
    }
}