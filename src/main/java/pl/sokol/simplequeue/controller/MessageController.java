package pl.sokol.simplequeue.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.sokol.simplequeue.config.QueueConfig;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MessageController {

    private final RabbitTemplate rabbitTemplate;

    @GetMapping(path = "/addMsg")
    public String addMessageToQueue(@RequestParam String msg) {
        log.info("Adding new message to queue, content: {}", msg);
        rabbitTemplate.convertAndSend(QueueConfig.MESSAGES_EXCHANGE_NAME, QueueConfig.MESSAGES_ROUTING_KEY_NAME, msg);
        return "SENT";
    }

    @GetMapping(path = "/health")
    public String health() {
        return "OK";
    }
}
